import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

import { CocktailService } from '../shared/cocktail.service';
import { Cocktail } from '../shared/cocktail.model';
import { FavoriteService } from '../shared/favorite.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  isLoading = false;
  isFavorite = false;
  drinkId: string;
  drink: Cocktail;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private cocktailService: CocktailService,
    private favoriteService: FavoriteService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('drinkId')) {
        this.navCtrl.navigateBack('/featured');
        return;
      }
      this.isLoading = true;
      this.drinkId = paramMap.get('drinkId');
    });
  }

  ionViewWillEnter() {
    this.favoriteService.fetchFavorites().subscribe((favorites) => {
      this.cocktailService.fetchCocktail(this.drinkId).subscribe((cocktail) => {
        this.drink = cocktail;
        this.isFavorite = favorites.find(({ drinkId }) => drinkId === this.drink.id)
          ? true
          : false;
        this.isLoading = false;
      });
    });
  }

  onFavorite() {
    if (!this.isFavorite) {
      this.favoriteService.favoriteCocktail(
        this.drink.id,
        this.drink.name,
        this.drink.imageUrl
      );
    } else {
      this.favoriteService.removeFavorite(this.drink.id);
    }
    this.isFavorite = !this.isFavorite;
  }
}
