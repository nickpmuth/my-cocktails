import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MenuController } from '@ionic/angular';

import { CocktailService } from '../shared/cocktail.service';
import { Cocktail } from '../shared/cocktail.model';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.page.html',
  styleUrls: ['./featured.page.scss'],
})
export class FeaturedPage implements OnInit, OnDestroy {
  featuredDrinks: Cocktail[];
  private drinkSub: Subscription;

  constructor(
    private cocktailService: CocktailService,
    private router: Router,
    private menuCtrl: MenuController
  ) {}

  ngOnInit() {
    this.drinkSub = this.cocktailService.featuredDrinks.subscribe(
      (featuredDrinks) => {
        this.featuredDrinks = featuredDrinks;
      }
    );
    this.cocktailService.getFeaturedCocktails();
  }

  onDrinkSelect(id: string) {
    this.router.navigate([`/detail/${id}`]);
  }

  refreshCocktailList() {
    this.cocktailService.refreshCocktailList();
  }

  doRefresh(event) {
    this.cocktailService
      .refreshCocktailList()
      .then(() => event.target.complete());
  }

  ngOnDestroy() {
    if (this.drinkSub) {
      this.drinkSub.unsubscribe();
    }
  }
}
