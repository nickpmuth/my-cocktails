import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap, map, tap, take } from 'rxjs/operators';

import { Cocktail, Ingredient } from './cocktail.model';
import { BehaviorSubject } from 'rxjs';
import { resolve } from 'dns';

interface CocktailData {
  idDrink: string;
  strDrink: string;
  strDrinkThumb: string;
  strInstructions: string;
}

@Injectable({
  providedIn: 'root',
})
export class CocktailService {
  private _featuredDrinks = new BehaviorSubject<Cocktail[]>([]);

  get featuredDrinks() {
    return this._featuredDrinks.asObservable();
  }

  constructor(private http: HttpClient) {}

  fetchCocktail(id: string) {
    return this.http
      .get<{ drinks: CocktailData[] }>(
        `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`
      )
      .pipe(
        map((resData) => {
          let fetchedDrink = resData.drinks[0];
          return new Cocktail(
            fetchedDrink.idDrink,
            fetchedDrink.strDrink,
            fetchedDrink.strDrinkThumb,
            this.mapIngredients(fetchedDrink),
            fetchedDrink.strInstructions
          );
        })
      );
  }

  mapIngredients(fetchedDrink: any): Ingredient[] {
    let ingredients: Ingredient[] = [];
    for (const key in fetchedDrink) {
      if (fetchedDrink.hasOwnProperty(key)) {
        if (key.includes('Ingredient') && fetchedDrink[key]) {
          let keyIndex = key.replace('strIngredient', '');
          const newIngredient: Ingredient = {
            name: fetchedDrink[key],
            measurement: fetchedDrink[`strMeasure${keyIndex}`],
          };
          ingredients.push(newIngredient);
        }
      }
    }
    return ingredients;
  }

  refreshCocktailList() {
    this._featuredDrinks.next([]);
    return this.getFeaturedCocktails();
  }

  async getFeaturedCocktails() {
    for (let i = 0; i <= 7; i++) {
      this.fetchRandomCocktail().subscribe();
    }
  }

  fetchRandomCocktail() {
    let newDrink: Cocktail;
    return this.http
      .get<{ drinks: CocktailData[] }>(
        'https://www.thecocktaildb.com/api/json/v1/1/random.php'
      )
      .pipe(
        switchMap((resData) => {
          const fetchedDrink = resData.drinks[0];
          newDrink = new Cocktail(
            fetchedDrink.idDrink,
            fetchedDrink.strDrink,
            fetchedDrink.strDrinkThumb,
            this.mapIngredients(fetchedDrink),
            fetchedDrink.strInstructions
          );
          return this.featuredDrinks;
        }),
        take(1),
        tap((featuredDrinks) => {
          this._featuredDrinks.next(featuredDrinks.concat(newDrink));
        })
      );
  }
}
