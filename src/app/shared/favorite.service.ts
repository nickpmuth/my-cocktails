import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map, switchMap, tap, take } from 'rxjs/operators';

import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class FavoriteService {
  private _favorites = new BehaviorSubject<
    { id: string; drinkId: string; name: string; imageUrl: string }[]
  >([]);

  get favorites() {
    return this._favorites.asObservable();
  }

  constructor(private http: HttpClient, private authService: AuthService) {}

  fetchFavorites() {
    return this.http
      .get<{ [key: string]: string }>(
        `https://my-cocktail-78101.firebaseio.com/favorite/${this.authService.userId}.json`
      )
      .pipe(
        map((resData) => {
          const favorites = [];
          for (const key in resData) {
            if (resData.hasOwnProperty(key)) {
              favorites.push({
                id: key,
                drinkId: resData[key]['id'],
                name: resData[key]['name'],
                imageUrl: resData[key]['imageUrl'],
              });
            }
          }
          return favorites;
        }),
        tap((favorites) => this._favorites.next(favorites))
      );
  }

  removeFavorite(drinkId: string) {
    let dbId: string;
    this.favorites.pipe(take(1)).subscribe((favorites) => {
      let drink = favorites.find((drink) => drink.drinkId === drinkId);
      if (!drink) {
        return;
      }
      dbId = drink['id'];
    });
    return this.http
      .delete(
        `https://my-cocktail-78101.firebaseio.com/favorite/${this.authService.userId}/${dbId}.json`
      )
      .subscribe();
  }

  favoriteCocktail(drinkId: string, name: string, imageUrl: string) {
    let dbId: string;
    return this.http
      .post<{ name: string }>(
        `https://my-cocktail-78101.firebaseio.com/favorite/${this.authService.userId}.json`,
        {
          id: drinkId,
          name: name,
          imageUrl: imageUrl,
        }
      )
      .pipe(
        switchMap((resData) => {
          dbId = resData.name;
          return this.favorites;
        }),
        take(1),
        tap((favorites) => {
          this._favorites.next(
            favorites.concat({
              id: dbId,
              drinkId: drinkId,
              name: name,
              imageUrl: imageUrl,
            })
          );
        })
      )
      .subscribe();
  }
}
