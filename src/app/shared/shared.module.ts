import { NgModule } from '@angular/core';

import { CocktailService } from './cocktail.service';

@NgModule({
    exports: [CocktailService]
})
export class SharedModule {

}