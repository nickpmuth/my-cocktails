export class Cocktail {
  constructor(
    public id: string,
    public name: string,
    public imageUrl: string,
    public ingredients: Ingredient[],
    public instructions: string
  ) {}
}

export interface Ingredient {
  name: string;
  measurement: string;
}
