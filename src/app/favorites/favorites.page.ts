import { Component, OnInit } from '@angular/core';

import { FavoriteService } from '../shared/favorite.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {
  favorites: { id: string; drinkId: string; name: string; imageUrl: string }[];

  constructor(private favoriteService: FavoriteService) {}

  ngOnInit() {
    this.favoriteService.favorites.subscribe((favorites) => {
      this.favorites = favorites;
    });
  }

  ionViewWillEnter() {
    this.favoriteService.fetchFavorites().subscribe();
  }
}
